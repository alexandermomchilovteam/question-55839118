import Cocoa
import RxSwift

func createTicker(interval: RxTimeInterval) -> Observable<Int> {
	return Observable<Int>
		.interval(interval, scheduler: MainScheduler.instance)
}

// This is intentionally really verbose, I know this code can be parameterized and combined.
// I'm showing how multiple types can be implemented to do different things
// (although they're similar in this example, just for demonstration),
// but exposing observables that can be handled in agggregate to achieve a greater effect.
struct RawDataSource1: ObservableConvertibleType {
	var observable: Observable<Int>
	
	init() {
		self.observable = Observable
			.zip(
				Observable.from(1...100),
				createTicker(interval: 0.1)
			)
			.map { (number, tickNumber) in number }
	}
	
	func asObservable() -> Observable<Int> {
		return self.observable
	}
}

struct RawDataSource2: ObservableConvertibleType {
	var observable: Observable<Int>
	
	init() {
		self.observable = Observable
			.zip(
				Observable.from(1...100),
				createTicker(interval: 0.2)
			)
			.map { (number, tickNumber) in number }
	}
	
	func asObservable() -> Observable<Int> {
		return self.observable
	}
}

struct RawDataSource3: ObservableConvertibleType {

	typealias E = Int
	
	
	var observable: Observable<Int>
	
	init() {
		self.observable = Observable
			.zip(
				Observable.from(1...100),
				createTicker(interval: 0.3)
			)
			.map { (number, tickNumber) in number }
	}
	
	func asObservable() -> Observable<Int> {
		return self.observable
	}
}

struct MyExampleModel {
	let i1: Int
	let i2: Int
	let i3: Int
	
	init?(i1: Int, i2: Int, i3: Int) {
		self.i1 = i1
		self.i2 = i3
		self.i3 = i3
		
		guard self.validate() else { return nil }
	}
	
	var product: Int { return i1 * i2 * i3 }
	
	// A really arbitrary example.
	func validate() -> Bool {
		return self.product.isMultiple(of: 12)
	}
}

struct MyExampleDataValidator: ObservableConvertibleType {
	private let rawDataSource1: RawDataSource1
	private let rawDataSource2: RawDataSource2
	private let rawDataSource3: RawDataSource3
	
	public let observable: Observable<MyExampleModel>
	
	init (
		rawDataSource1: RawDataSource1,
		rawDataSource2: RawDataSource2,
		rawDataSource3: RawDataSource3
	) {
		self.rawDataSource1 = rawDataSource1
		self.rawDataSource2 = rawDataSource2
		self.rawDataSource3 = rawDataSource3
		
		self.observable = Observable.combineLatest(
			rawDataSource1.asObservable(),
			rawDataSource2.asObservable(),
			rawDataSource3.asObservable()
		)
		.flatMap { (tuple: (i1: Int, i2: Int, i3: Int)) -> Observable<MyExampleModel> in
			if let model = MyExampleModel(i1: tuple.i1, i2: tuple.i2, i3: tuple.i3) {
				print("Created validated model: \(model) whose product is \(model.product), which is a multiple of 12")
				return Observable.just(model)
			} else {
				print("The tuple \(tuple) does not produce a valid model.")
				return Observable.empty()
			}
			
			// If this if/else didn't exist for the logging in this example, we could just:
			// return Observable.from(optional: model)
		}
	}
	
	func asObservable() -> Observable<MyExampleModel> {
		return self.observable
	}
}


struct MyExampleView: ObserverType {
	func on(_ event: Event<MyExampleModel>) {
		switch event {
		case .next(let model):
			print(model)
		case .error(let error):
			print("Error! \(error)")
		case .completed:
			print("All done here!")
		}
	}
}

struct MyExampleController {
	let view = MyExampleView()
	let model = MyExampleDataValidator(
		rawDataSource1: RawDataSource1(),
		rawDataSource2: RawDataSource2(),
		rawDataSource3: RawDataSource3()
	)
	
	// The controller doesn't do much, it just sets up the bindings, one time
	init() {
		self.model.asObservable().subscribe(self.view)
	}
}


// Usually it's your storyboard that instantiates controllers and keeps them alive
// but here I do it with a simple variable for demo purposes.
let myController = MyExampleController()

import PlaygroundSupport
PlaygroundPage.current.needsIndefiniteExecution
